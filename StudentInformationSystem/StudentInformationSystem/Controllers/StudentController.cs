﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntitesViewModel.ViewModel;
using BusinessLayer;
using DAL.DATA;

namespace StudentInformationSystem.Controllers
{
    public class StudentController : BaseController
    {
        UnitOfWork unitofwork = new UnitOfWork();

        //
        // GET: /Student/

        public ActionResult Index()
        {


            //var s = unitofwork.StudentRepository.Get().FirstOrDefault();
            //List<tblStudentHistory> studenthistorylist = unitofwork.StudentHistoryRepository.Get(filter: p => p.student_id == s.student_id).ToList();
            StudentViewModel studentviewmodel = new StudentViewModel();
            //studentviewmodel.Student = s;
            //studentviewmodel.StudentHistory = studenthistorylist;

         
            return View(studentviewmodel);
          
        }

        public ActionResult Test()
        {
            return View();
        }
        public ActionResult ViewHistory()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        public ActionResult ViewMySubjects()
        {
            return View();
        }


    }
}
