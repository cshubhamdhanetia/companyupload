﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.DATA;
using BusinessLayer;
using EntitesViewModel;
using EntitesViewModel.ViewModel;

namespace StudentInformationSystem.Controllers
{
    public class AdminController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddStudent()
        {

            StudentDetailViewModel studentDetailViewModel = new StudentDetailViewModel();
            ViewBag.city_id = new SelectList(unitOfWork.CityRepository.Get(), "city_id", "city_name");
            ViewBag.state_id = new SelectList(unitOfWork.StateRepository.Get(), "state_id", "state_name");
            ViewBag.university_id = new SelectList(unitOfWork.UniversityRepository.Get(), "university_id", "university_name");
            ViewBag.college_id = new SelectList(unitOfWork.CollegeRepository.Get(), "college_id", "college_name");
            ViewBag.sem_id = new SelectList(unitOfWork.SemesterRepository.Get(), "sem_id", "sem_no");
            ViewBag.department_id = new SelectList(unitOfWork.DepartmentRepository.Get(), "department_id", "department_name");
            ViewBag.role_id = new SelectList(unitOfWork.RolesRepository.Get(), "role_id", "role_name");
            return View(studentDetailViewModel);

        }
        [HttpPost]
        public ActionResult AddStudent(StudentDetailViewModel studentModel)
        {
            if (ModelState.IsValid)
            {
                tblStudent tblstudent = new tblStudent();
                tblStudentHistory tblstudenthistory = new tblStudentHistory();
                tblUserInfo tbluserinfo = new tblUserInfo();

                tblstudent = studentModel.Student;             
                tblstudent.student_id = Guid.NewGuid();



                tblstudenthistory= studentModel.StudentHistory;
                tblstudenthistory.LogDate = DateTime.Now;
                tblstudenthistory.id = Guid.NewGuid();
                tblstudenthistory.student_id = tblstudent.student_id;

                unitOfWork.StudentRepository.Insert(tblstudent);         
                unitOfWork.StudentHistoryRepository.Insert(tblstudenthistory);

                tbluserinfo.id = Guid.NewGuid();
                tbluserinfo.email = studentModel.Student.student_email;
                tbluserinfo.password = studentModel.Student.student_dateofbirth.ToString("dd-MM-yyyy");
                tbluserinfo.role_id = studentModel.role_id;
                unitOfWork.UserInfoRepository.Insert(tbluserinfo);

                unitOfWork.Save();
               
               

                return RedirectToAction("Index");
            }


            return View();
        }

        public ActionResult Test()
        {
            StudentDetailViewModel studentDetailViewModel = new StudentDetailViewModel();
            ViewBag.city_id = new SelectList(unitOfWork.CityRepository.Get(), "city_id", "city_name");
            ViewBag.state_id = new SelectList(unitOfWork.StateRepository.Get(), "state_id", "state_name");
            ViewBag.university_id = new SelectList(unitOfWork.UniversityRepository.Get(), "university_id", "university_name");
            ViewBag.college_id = new SelectList(unitOfWork.CollegeRepository.Get(), "college_id", "college_name");
            ViewBag.sem_id = new SelectList(unitOfWork.SemesterRepository.Get(), "sem_id", "sem_no");
            ViewBag.department_id = new SelectList(unitOfWork.DepartmentRepository.Get(), "department_id", "department_name");
            return View(studentDetailViewModel);

        }
        public ActionResult ViewStudents()
        {
            
            List<tblStudent> student_list = unitOfWork.StudentRepository.Get().ToList();
            List<StudentViewModel> studentviewmodellist = new List<StudentViewModel>();
            foreach(tblStudent tblstudent in student_list)
            {
                StudentViewModel studentviewmodel = new StudentViewModel();
                tblStudentHistory studenthistory = new tblStudentHistory();
                studentviewmodel.Student = tblstudent;
                studenthistory = unitOfWork.StudentHistoryRepository.Get(filter: p => p.student_id == tblstudent.student_id).LastOrDefault();
                studentviewmodel.sem_number = studenthistory.sem_id.ToString();
                studentviewmodel.college_name = studenthistory.tblCollege.college_name;
                studentviewmodel.department_name = studenthistory.tblDepartment.department_name;
                studentviewmodellist.Add(studentviewmodel);
            }
            
            return View(studentviewmodellist.ToList());
        }

    }
}
