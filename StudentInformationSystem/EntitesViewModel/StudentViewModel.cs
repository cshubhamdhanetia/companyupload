﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.DATA;


namespace EntitesViewModel.ViewModel
{
    public class StudentViewModel
    {
        public tblStudent Student { get; set; }
        public List<tblStudentHistory> StudentHistory { get; set; }
        public string college_name { get; set; }
        public string sem_number { get; set; }
        public string department_name { get; set; }

        public StudentViewModel()
        {
            Student = new tblStudent();
            StudentHistory = new List<tblStudentHistory>();
        }
    }
 
}
