﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitesViewModel
{
    public class StudentDetailViewModel
    {
        public tblStudent Student { get; set; }
        public tblStudentHistory StudentHistory { get; set; }
        public int role_id { get; set; }

        public StudentDetailViewModel()
        {
            Student = new tblStudent();
            StudentHistory = new tblStudentHistory();
        }
    }
}
