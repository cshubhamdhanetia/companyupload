﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DATA;
using DAL.Repository;
using DAL.Repository.Repositories;

namespace BusinessLayer
{
    public class UnitOfWork : IDisposable
        {
          
            private StudentInformationSystemEntities context = new StudentInformationSystemEntities();

            private StudentRepository<tblStudent> _studentRepository;
            private CollegeDepartmentRepository<tblCollegeDepartment> _collegeDepartmentRepository;
            private CollegeRepository<tblCollege> _collegeRepository;
            private DepartmentSubjectRepository<tblDepartmentSubject> _departmentSubjectRepository;
            private RolesRepository<tblRole> _rolesRepository;
            private StudentHistoryRepository<tblStudentHistory> _studentHistoryRepository;
            private SubjectRepository<tblSubject> _subjectRepository;
            private UniversityRepository<tblUniversity> _universityRepository;
            private UserInfoRepository<tblUserInfo> _userInfoRepository;
            private DepartmentRepository<tblDepartment> _departmentRepository;
            
        private GenericRepository<tblCity> _cityRepository;
        private GenericRepository<tblState> _stateRepository;
        private GenericRepository<tblSemester> _semesterRepository;

        public StudentRepository<tblStudent> StudentRepository
            {
                get
                {

                    if (this._studentRepository == null)
                    {
                        this._studentRepository = new StudentRepository<tblStudent>(context);
                    }
                    return _studentRepository;
                }
            }

        public GenericRepository<tblSemester> SemesterRepository
        {
            get
            {

                if (this._semesterRepository == null)
                {
                    this._semesterRepository = new GenericRepository<tblSemester>(context);
                }
                return _semesterRepository;
            }
        }

        public GenericRepository<tblCity> CityRepository
        {
            get
            {

                if (this._cityRepository == null)
                {
                    this._cityRepository = new GenericRepository<tblCity>(context);
                }
                return _cityRepository;
            }
        }

        public GenericRepository<tblState> StateRepository
        {
            get
            {

                if (this._stateRepository == null)
                {
                    this._stateRepository = new GenericRepository<tblState>(context);
                }
                return _stateRepository;
            }
        }
        public void Save()
        {            
                context.SaveChanges();
            
        }




        public CollegeDepartmentRepository<tblCollegeDepartment> CollegeDepartmentRepository
            {
                get
                {

                    if (this._collegeDepartmentRepository == null)
                    {
                        this._collegeDepartmentRepository = new CollegeDepartmentRepository<tblCollegeDepartment>(context);
                    }
                    return _collegeDepartmentRepository;
                }
            }

            public CollegeRepository<tblCollege> CollegeRepository
            {
                get
                {

                    if (this._collegeRepository == null)
                    {
                        this._collegeRepository = new CollegeRepository<tblCollege>(context);
                    }
                    return _collegeRepository;
                }
            }

            public DepartmentSubjectRepository<tblDepartmentSubject> DepartmentSubjectRepository
            {
                get
                {

                    if (this._departmentSubjectRepository == null)
                    {
                        this._departmentSubjectRepository = new DepartmentSubjectRepository<tblDepartmentSubject>(context);
                    }
                    return _departmentSubjectRepository;
                }
            }

            public RolesRepository<tblRole> RolesRepository
            {
                get
                {

                    if (this._rolesRepository == null)
                    {
                        this._rolesRepository = new RolesRepository<tblRole>(context);
                    }
                    return _rolesRepository;
                }
            }

            public StudentHistoryRepository<tblStudentHistory> StudentHistoryRepository
            {
                get
                {

                    if (this._studentHistoryRepository == null)
                    {
                        this._studentHistoryRepository = new StudentHistoryRepository<tblStudentHistory>(context);
                    }
                    return _studentHistoryRepository;
                }
            }

            public SubjectRepository<tblSubject> SubjectRepository
            {
                get
                {

                    if (this._subjectRepository == null)
                    {
                        this._subjectRepository = new SubjectRepository<tblSubject>(context);
                    }
                    return _subjectRepository;
                }
            }

            public UniversityRepository<tblUniversity> UniversityRepository
            {
                get
                {

                    if (this._universityRepository == null)
                    {
                        this._universityRepository = new UniversityRepository<tblUniversity>(context);
                    }
                    return _universityRepository;
                }
            }

            public UserInfoRepository<tblUserInfo> UserInfoRepository
            {
                get
                {

                    if (this._userInfoRepository == null)
                    {
                        this._userInfoRepository = new UserInfoRepository<tblUserInfo>(context);
                    }
                    return _userInfoRepository;
                }
            }

            public DepartmentRepository<tblDepartment> DepartmentRepository
            {
                get
                {

                    if (this._departmentRepository == null)
                    {
                        this._departmentRepository = new DepartmentRepository<tblDepartment>(context);
                    }
                    return _departmentRepository;
                }
            }






            private bool disposed = false;
            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        context.Dispose();
                    }
                }
                this.disposed = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

  
}
   