﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class CollegeRepository<TEntity> : GenericRepository<TEntity>, ICollegeRepository where TEntity : class
    {
        public CollegeRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}
