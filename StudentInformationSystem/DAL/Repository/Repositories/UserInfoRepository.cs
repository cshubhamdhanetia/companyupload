﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class UserInfoRepository<TEntity> : GenericRepository<TEntity>, IUserinfoRepository where TEntity : class
    {
        public UserInfoRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }


    }
}
