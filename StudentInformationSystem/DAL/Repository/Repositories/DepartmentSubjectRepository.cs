﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class DepartmentSubjectRepository<TEntity> : GenericRepository<TEntity>, IDepartmentSubjectRepository where TEntity : class
    {
        public DepartmentSubjectRepository(StudentInformationSystemEntities context)
            : base(context)
        {


        }

    }
}
