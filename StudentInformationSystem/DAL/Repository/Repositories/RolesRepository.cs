﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class RolesRepository<TEntity> : GenericRepository<TEntity>, IRolesRepository where TEntity : class
    {
        public RolesRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}
