﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class UniversityRepository<TEntity> : GenericRepository<TEntity>, IUniversityRepository where TEntity : class
    {
        public UniversityRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}
