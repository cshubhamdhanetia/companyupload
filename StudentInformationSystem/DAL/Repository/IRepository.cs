﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        TEntity GetByID(object id);
        void Update(TEntity entityToUpdate);
        void Delete(object id);
        void Delete(TEntity entityToDelete);
        void Insert(TEntity entity);

    }
    public interface ICollegeRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface ICollegeDepartmentRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IDepartmentRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IDepartmentSubjectRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IRolesRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IStudentRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IStudentHistoryRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface ISubjectRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IUniversityRepository
    {
        //IEnumerable GetProductsByCategory(int id);
    }
    public interface IUserinfoRepository
    {

        //IEnumerable GetProductsByCategory(int id);
    }
}
