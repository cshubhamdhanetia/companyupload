//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSubject
    {
        public tblSubject()
        {
            this.tblDepartmentSubjects = new HashSet<tblDepartmentSubject>();
        }
    
        public int subject_id { get; set; }
        public string subject_code { get; set; }
        public string subject_name { get; set; }
    
        public virtual ICollection<tblDepartmentSubject> tblDepartmentSubjects { get; set; }
    }
}
