//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSemester
    {
        public tblSemester()
        {
            this.tblStudentHistories = new HashSet<tblStudentHistory>();
        }
    
        public int sem_Id { get; set; }
        public int sem_no { get; set; }
    
        public virtual ICollection<tblStudentHistory> tblStudentHistories { get; set; }
    }
}
